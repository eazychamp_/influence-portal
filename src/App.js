import './App.css';
import React from "react";
import Chart from "react-google-charts";


const App = () => {
  return (
    <div className='wrapper'>
		<div className='sidebar'>
			<ul>
				<li>
					<a href='#'>
						<span class='icon'>
							<i class='fas fa-desktop'></i>
						</span>
						<span class='item'>My Dashboard</span>
					</a>
				</li>
				<li>
					<a href='#'>
						<span class='icon'>
							<i class='fas fa-user-friends'></i>
						</span>
						<span class='item'>Explorer</span>
					</a>
				</li>
				<li>
					<a href='#'>
						<span class='icon'>
							<i class='fas fa-tachometer-alt'></i>
						</span>
						<span class='item'>Influencer</span>
					</a>
				</li>
				<li>
					<a href='#'>
						<span class='icon'>
							<i class='fas fa-database'></i>
						</span>
						<span class='item'>Help</span>
					</a>
				</li>
				<li>
					<a href='#'>
						<span class='icon'>
							<i class='fas fa-chart-line'></i>
						</span>
						<span class='item'>Profile</span>
					</a>
				</li>
				<li>
					<a href='#'>
						<span class='icon'>
							<i class='fas fa-user-shield'></i>
						</span>
						<span class='item'>Contact us</span>
					</a>
				</li>
				<li>
					<a href='#'>
						<span class='icon'>
							<i class='fas fa-cog'></i>
						</span>
						<span class='item'>Logout</span>
					</a>
				</li>
			</ul>
		</div>
		<div className='rightPannel'>
			<div style={{ display: "flex", maxWidth: 1000 }}>
				<Chart
					width={530}
					height={400}
					chartType='ColumnChart'
					loader={<div>Loading Chart</div>}
					data={[
						["City", "2010 Population", "2000 Population"],
						["New York City, NY", 8175000, 8008000],
						["Los Angeles, CA", 3792000, 3694000],
						["Chicago, IL", 2695000, 2896000],
						["Houston, TX", 2099000, 1953000],
						["Philadelphia, PA", 1526000, 1517000],
					]}
					options={{
						title: "Population of Largest U.S. Cities",
						chartArea: { width: "30%" },
						hAxis: {
							title: "Total Population",
							minValue: 0,
						},
						vAxis: {
							title: "City",
						},
					}}
					legendToggle
				/>
				<Chart
					width={530}
					height={"400px"}
					chartType='AreaChart'
					loader={<div>Loading Chart</div>}
					data={[
						["Year", "Sales", "Expenses"],
						["2013", 1000, 400],
						["2014", 1170, 460],
						["2015", 660, 1120],
						["2016", 1030, 540],
					]}
					options={{
						title: "Company Performance",
						hAxis: { title: "Year", titleTextStyle: { color: "#333" } },
						vAxis: { minValue: 0 },
						// For the legend to fit, we make the chart area smaller
						chartArea: { width: "50%", height: "70%" },
						// lineWidth: 25
					}}
				/>
			</div>
			<div style={{ display: "flex" }}>
				<Chart
					width={530}
					height={"400px"}
					chartType='BubbleChart'
					loader={<div>Loading Chart</div>}
					data={[
						[
							"ID",
							"Life Expectancy",
							"Fertility Rate",
							"Region",
							"Population",
						],
						["CAN", 80.66, 1.67, "North America", 33739900],
						["DEU", 79.84, 1.36, "Europe", 81902307],
						["DNK", 78.6, 1.84, "Europe", 5523095],
						["EGY", 72.73, 2.78, "Middle East", 79716203],
						["GBR", 80.05, 2, "Europe", 61801570],
						["IRN", 72.49, 1.7, "Middle East", 73137148],
						["IRQ", 68.09, 4.77, "Middle East", 31090763],
						["ISR", 81.55, 2.96, "Middle East", 7485600],
						["RUS", 68.6, 1.54, "Europe", 141850000],
						["USA", 78.09, 2.05, "North America", 307007000],
					]}
					options={{
						title:
							"Correlation between life expectancy, fertility rate " +
							"and population of some world countries (2010)",
						hAxis: { title: "Life Expectancy" },
						vAxis: { title: "Fertility Rate" },
						bubble: { textStyle: { fontSize: 11 } },
					}}
				/>
				<Chart
					width={530}
					height={400}
					chartType='LineChart'
					loader={<div>Loading Chart</div>}
					data={[
						["x", "dogs", "cats"],
						[0, 0, 0],
						[1, 10, 5],
						[2, 23, 15],
						[3, 17, 9],
						[4, 18, 10],
						[5, 9, 5],
						[6, 11, 3],
						[7, 27, 19],
					]}
					options={{
						hAxis: {
							title: "Time",
						},
						vAxis: {
							title: "Popularity",
						},
						series: {
							1: { curveType: "function" },
						},
					}}
				/>
			</div>
			<div style={{ display: "flex" }}>
				<Chart
					width={"500px"}
					height={"300px"}
					chartType='GeoChart'
					data={[
						["Country", "Popularity"],
						["Germany", 200],
						["United States", 300],
						["Brazil", 400],
						["Canada", 530],
						["France", 600],
						["RU", 700],
					]}
					// Note: you will need to get a mapsApiKey for your project.
					// See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
					mapsApiKey='YOUR_KEY_HERE'
					rootProps={{ "data-testid": "1" }}
				/>
			</div>
		</div>
	</div>

  );
}

export default App;
